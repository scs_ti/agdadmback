# Agenda administrativa (BACKEND)

## Para rodar esse projeto:

1. Instale as dependências com o comando:
```
    npm i
```

2. Rode as migrações do banco de dados com o comando:
```
    npm run typeorm migration:run
```

3. Inicie a aplicação com o comando:
```
    npm run start
```

4. Criar uma nova migration:
```
    yarn typeorm migration:generate -n NOME DA MIGRATION
```

## Rotas

### Usuários

- {baseURL}/auth/login
- {baseURL}/colaborador/:username
- {baseURL}/colaborador/teste/:username

### Unidades

- {baseURL}/unidade/create
- {baseURL}/unidade/list
- {baseURL}/unidade/find/:id
- {baseURL}/unidade/update/:id
- {baseURL}/unidade/delete/:id

### Arquivos

- {baseURL}/arquivo/upload
- {baseURL}/arquivo/download/:id

### Agendamentos

- {baseURL}/agendamento/create
- {baseURL}/agendamento/list
- {baseURL}/agendamento/find/:id
- {baseURL}/agendamento/clbd/:username
- {baseURL}/agendamento/update/:id
- {baseURL}/agendamento/delete/:id